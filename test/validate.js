// Dropzone.options.myAwesomeDropzone = {
//   maxFiles: 1,
//   acceptedFiles: ".png, .jpeg, .gif, .bmp",
//   autoProcessQueue: false,
//   init: function() {
//       var self = this;
//       //New file added
//       self.on("addedfile", function(file) {
//         console.log("new file added ", file);
//       });
//       // Send file starts
//       self.on("maxfilesexceeded", function(file, response) {
//         this.removeFile(file);
//       });

//       self.on("addedfile", function(file) {
//         const pattern = /\d{6}(\.)(jpg|jpeg|png)/;

//         if (!pattern.test(file.name)) {
//           //   this.removeFile(file);
//         }
//       });
//     },
  
// }
Dropzone.options.myAwesomeDropzone = {
    thumbnailHeight: 210,
    thumbnailWidth: 140,
    maxFilesize: 5,
    autoProcessQueue: false,
    maxFiles: 1,
    dictResponseError: "Server not Configured",
      dictFileTooBig: "File too big ({{filesize}}MB). Must be less than {{maxFilesize}}MB.",
    dictCancelUpload: "",
    acceptedFiles: ".png,.jpg,.jpeg",
    init: function() {
      var self = this;
      //New file added
      self.on("addedfile", function(file) {
        console.log("new file added ", file);
      });
      self.on("maxfilesexceeded", function(file, response) {
        this.removeFile(file);
      });

      self.on("addedfile", function(file) {
        const pattern = /\d{6}(\.)(jpg|jpeg|png)/;

        if (!pattern.test(file.name)) {
          //   this.removeFile(file);
        }
      });
    },
    // accept: function(file, done) {
    //   const pattern = /\d{6}(\.)/;

    //   if (pattern.test(file.name)) {
    //     done();
    //   } else {
    //     done("File name not a valid admission number");
    //     return false;
    //   }
    // },

    
    previewTemplate: `
<div class="dz-preview dz-file-preview">
  <div class="dz-image"><img data-dz-thumbnail /></div>
  <div class="dz-error-message"><i class="fa fa-warning">&nbsp;</i><span data-dz-errormessage></span></div>
  <div class="dz-filename"><span data-dz-name></span></div>
  <div class="dz-progress">
    <span class="dz-upload" data-dz-uploadprogress></span>
  </div>
  <div class="dz-remove">
    <a href="javascript:undefined;" data-dz-remove=""><i class="fa fa-trash-o"></i>&nbsp;<span>Remove</span></div>
</div>
`
  };


